const gulp = require('gulp')
const mjml = require('gulp-mjml')

gulp.task('build', function () {
    return gulp.src('source/*/*.mjml')
        .pipe(mjml())
        .pipe(gulp.dest('output/'))
});

gulp.task('watch', function (){
    gulp.watch(['./source/*/*.mjml'], gulp.series('build'));
});